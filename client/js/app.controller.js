// purpose is to split the codes into smaller pieces

(function(){
    "use strict"
    angular.module ("PopQuizApp").controller("PopQuizCtrl", PopQuizCtrl);

// remove the square bracker for PopQuizCtrl and add on the following line. A design patter of angular

    PopQuizCtrl.$inject = ["$http"];
// in the injection function, it works as an initialiser, identify as a string in the inject command
// will reflect back from server as an object which do not require the ""
    function PopQuizCtrl($http){
            var self = this;
            self.quiz = {

            };
            self.finalAnswer = {
                choice:"",
                remarks: "",
            }
            self.isCorrect = ""
// the following question not valid cause quizes.json (server side) already have it hard coded
//            self.question = "What would you like to eat today???";
            self.initForm = function(){
                $http.get("/api/popquizes").then((result)=>{
                    console.log(result);
                    self.quiz = result.data;
                }).catch((e)=>{
                    console.log(e);
// use .then (()=>{
//}).catch(()=>{}) when there is uncertainty in the bandwidth of response
                });
            }
            self.initForm();
            self.submitQuiz = ()=>{
                console.log(self.finalAnswer.choice);
                console.log(self.finalAnswer.remarks);
                $http.post("/api/submitQuizes",self.finalAnswer).then((result)=>{
                    self.isCorrect = result.data;
                }).catch((e)=>{
                    console.log(e);
                });
            }
    }

})();

// in angular, $http is a service that you can use to communicate between server and client. 
// for the client side to call out the express store in server
// google angular js.1 $http