/*
this is client side app.js, change to app.module.js
this is  angular.js
*/


// this is iife. put it at the start of the app.js
(function(){
    "use strict";
    //    var app = 
    angular.module ("PopQuizApp",[]);
/* app is the entire web application. this is the initialisation of the web app. this is done once in
beginning [] - a dependency in angular, a design pattern, DI (dependency Injection) is used to tightly coupled 2
modules so that you can use the other party's program, software.

after splitting into module and controller, remove the following lines

    app.controller("PopQuizCtrl", [PopQuizCtrl])

this controller is for the client side, "identifier of controller", [dependency of controller].
this will be repeated many times to control movement between the pages of the web. 


    function PopQuizCtrl(){
            var self = this;

            self.question = "What would you like to eat today???";
    }
function is a class/object. this signify the controller
*/
})();
