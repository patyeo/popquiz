/*
* this app.js is server side
* Express middleware
*/

var express = require("express");
var app = express();
var bodyParser = require("body-parser");

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

//this is to tell server to convert data key in client side to json file, so that it can read.
// and limit the size to 50mb to prevent hacking
app.use(bodyParser.urlencoded({limit:'50mb', extended:true}));
app.use(bodyParser.json({limit:'50mb'}));

var popQuiz = require("./quizes.json");
console.log(popQuiz);

app.get("/api/popquizes",(req,res)=>{
    console.log("get popquizes");
    res.status(200).json(popQuiz);
});
// '/api/popquizes' is a endpoint, a url so need to include '/' while server/app.js is an executable file
app.post("/api/submitQuizes",(req,res)=>{
    console.log("submit popquizes");
    console.log(req.body);
// convert body choice to integer.
    var choiceInt = parseInt(req.body.choice);
    var correctAnswer = parseInt(popQuiz.correctAnswer);
// check whether answer is correct
    if (choiceInt == correctAnswer){
        popQuiz.correctMessage = "It's correct!";
        console.log("It's correct!");
    }else {
        popQuiz.correctMessage = "It's incorrect!";
        console.log("It's in correct!");
    }
    res.status(200).json(popQuiz);
});
// created another end point for the answers to the quiz to be posted to

app.use((req,res)=>{
    var y = 6;
    try{
        console.log("Y : " + y);
        res.send(`<h1> Oopps wrong please ${y}</h1>`);
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
}); 